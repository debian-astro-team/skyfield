Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: skyfield
Upstream-Contact: Brandon Rhodes <brandon@rhodesmill.org>
Source: https://github.com/skyfielders/python-skyfield
Comment: The upstream release is repacked in order to exclude binary
 files with unclear license and files not used for packaging.
Files-Excluded: *.bsp
                *.pdf
                authorities
                bin
                builders
                ci
                containers
                hip_main.dat*
                documentation/_static/*.ttf

Files: *
Copyright: 2013–2018, Brandon Rhodes
License: Expat

Files: debian/*
Copyright: 2022-2025, Antonio Valentino <antonio.valentino@tiscali.it>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
